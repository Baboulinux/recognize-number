# Gestion des imports
import tensorflow as tf
import PIL
import imageio
import time
import numpy as np
import glob
import os
import matplotlib.pyplot as plt
from IPython import display
from tensorflow.keras import layers
import IPython




(imageAEntrainer, labelAEntrainer), (_, _) = tf.keras.datasets.mnist.load_data()

imageAEntrainer = imageAEntrainer.reshape(imageAEntrainer.shape[0], 28, 28, 1).astype('float32')
imageAEntrainer = (imageAEntrainer - 127.5) / 127.5 # Normalize the images to [-1, 1]

BUFFER_SIZE = 60000
BATCH_SIZE = 256

# Création du jeu de données entrainé
jeuDonneesEntraine = tf.data.Dataset.from_tensor_slices(imageAEntrainer).shuffle(BUFFER_SIZE).batch(BATCH_SIZE)

# Méthode permettant de créer la fenêtre affichant le résultat
def generationModele():
    modele = tf.keras.Sequential()
    modele.add(layers.Dense(7*7*256, use_bias=False, input_shape=(100,)))
    modele.add(layers.BatchNormalization())
    modele.add(layers.LeakyReLU())

    modele.add(layers.Reshape((7, 7, 256)))
    assert modele.output_shape == (None, 7, 7, 256) # Note: None is the batch size

    modele.add(layers.Conv2DTranspose(128, (5, 5), strides=(1, 1), padding='same', use_bias=False))
    assert modele.output_shape == (None, 7, 7, 128)
    modele.add(layers.BatchNormalization())
    modele.add(layers.LeakyReLU())

    modele.add(layers.Conv2DTranspose(64, (5, 5), strides=(2, 2), padding='same', use_bias=False))
    assert modele.output_shape == (None, 14, 14, 64)
    modele.add(layers.BatchNormalization())
    modele.add(layers.LeakyReLU())

    modele.add(layers.Conv2DTranspose(1, (5, 5), strides=(2, 2), padding='same', use_bias=False, activation='tanh'))
    assert modele.output_shape == (None, 28, 28, 1)

    return modele


generateur = generationModele()

bruit = tf.random.normal([1, 100])
imageGeneree = generateur(bruit, training=False)

plt.imshow(imageGeneree[0, :, :, 0], cmap='gray')

# Création d'un discriminateur permettan de déterminer si
# l'image est réelle ou fausse
# Si résultat bon --> valeur positive
# Si résultat faux --> valeur négative
def discriminateurModel():
    modele = tf.keras.Sequential()
    modele.add(layers.Conv2D(64, (5, 5), strides=(2, 2), padding='same',
                                     input_shape=[28, 28, 1]))
    modele.add(layers.LeakyReLU())
    modele.add(layers.Dropout(0.3))

    modele.add(layers.Conv2D(128, (5, 5), strides=(2, 2), padding='same'))
    modele.add(layers.LeakyReLU())
    modele.add(layers.Dropout(0.3))

    modele.add(layers.Flatten())
    modele.add(layers.Dense(1))

    return modele


discriminateur = discriminateurModel()
decision = discriminateur(imageGeneree)
print ("decision: ", decision)

# Cette méthode va faire une estimation future du résultat à obtenir
estimationFuture = tf.keras.losses.BinaryCrossentropy(from_logits=True)

# Méthode permettant de définir la perte du discriminateur pour optimiser le résultat
def perteDiscriminateur(vraiSortie, fausseSortie):
    vraiPerte = estimationFuture(tf.ones_like(vraiSortie), vraiSortie)
    faussePerte = estimationFuture(tf.zeros_like(fausseSortie), fausseSortie)
    totalPerte = vraiPerte + faussePerte
    return totalPerte

# Méthode permettant de définir la perte du générateur pour optimiser le résultat
def perteGenerateur(fausseSortie):
    return estimationFuture(tf.ones_like(fausseSortie), fausseSortie)

optimisationGenerateur = tf.keras.optimizers.Adam(1e-4)
optimisationDiscriminateur = tf.keras.optimizers.Adam(1e-4)

# Sauvegarde de l'image avec optimisation
dossierSauvegarde = './iterations'
prefix = os.path.join(dossierSauvegarde, "ckpt")
sauvegarde = tf.train.Checkpoint(optimisationGenerateur=optimisationGenerateur,
                                 optimisationDiscriminateur=optimisationDiscriminateur,
                                 generateur=generateur,
                                 discriminator=discriminateur)

Iteration = 50
dimensionBruit = 100
nbGenerationExemples = 16

# Création d'une graine pour animer les différents résultats
graine = tf.random.normal([nbGenerationExemples, dimensionBruit])


# Méthode permettant d'entraîner une étape de l'image
@tf.function
def entrainementParEtape(images):
    bruit = tf.random.normal([BATCH_SIZE, dimensionBruit])

    with tf.GradientTape() as bandeGeneratrice, tf.GradientTape() as bandeDiscriminatrice:
      imageGenerees = generateur(bruit, training=True)

      vraiSortie = discriminateur(images, training=True)
      fausseSortie = discriminateur(imageGenerees, training=True)

      perteGen = perteGenerateur(fausseSortie)
      perteDisc = perteDiscriminateur(vraiSortie, fausseSortie)

    gradientGenerateur = bandeGeneratrice.gradient(perteGen, generateur.trainable_variables)
    gradientDiscriminateur = bandeDiscriminatrice.gradient(perteDisc,  discriminateur.trainable_variables)

    optimisationGenerateur.apply_gradients(zip(gradientGenerateur, generateur.trainable_variables))
    optimisationDiscriminateur.apply_gradients(zip(gradientDiscriminateur, discriminateur.trainable_variables))

# Méthode d'entraînement de l'image
def apprentissage(jeuDonnees, Iteration):
  for iteration in range(Iteration):
    start = time.time()

    for lotImage in jeuDonnees:
      entrainementParEtape(lotImage)

    # Génération de l'image pour réaliser le GIF
    display.clear_output(wait=True)
    genererSauvegarderImage(generateur,
                             iteration + 1,
                             graine)

    # Sauvegar du model toute les 10 itérations
    if (iteration + 1) % 10 == 0:
      sauvegarde.save(file_prefix = prefix)

    print ('Le temps d\'une itération n°{} est de {} sec'.format(iteration + 1, time.time()-start))

  # Générer la sauvegarde de l'image après la dernière itération
  display.clear_output(wait=True)
  genererSauvegarderImage(generateur,
                           Iteration,
                           graine)

# Méthode permettant de générer et de sauvegarder l'image
def genererSauvegarderImage(modele, iteration, test_input):

  predictions = modele(test_input, training=False)

  fig = plt.figure(figsize=(4,4))

  for i in range(predictions.shape[0]):
      plt.subplot(4, 4, i+1)
      plt.imshow(predictions[i, :, :, 0] * 127.5 + 127.5, cmap='gray')
      plt.axis('off')

  plt.savefig('image_iteration_{:04d}.png'.format(iteration))
  plt.show()


apprentissage(jeuDonneesEntraine, Iteration)

sauvegarde.restore(tf.train.latest_checkpoint(dossierSauvegarde))

# Affichage d'une image en affichant le numéro de l'itération
def afficherImage(numIteration):
  return PIL.Image.open('image_iteration_{:04d}.png'.format(numIteration))

afficherImage(Iteration)

anim_file = 'dcgan.gif'

with imageio.get_writer(anim_file, mode='I') as writer:
  filenames = glob.glob('image*.png')
  filenames = sorted(filenames)
  last = -1
  for i,filename in enumerate(filenames):
    frame = 2*(i**0.5)
    if round(frame) > round(last):
      last = frame
    else:
      continue
    image = imageio.imread(filename)
    writer.append_data(image)
  image = imageio.imread(filename)
  writer.append_data(image)


if IPython.version_info > (6,2,0,''):
  display.Image(filename=anim_file)


try:
  from google.colab import files
except ImportError:
   pass
else:
  files.download(anim_file)